const theme = {
  spacing: (value = 1) => value * 8
}

export default theme
