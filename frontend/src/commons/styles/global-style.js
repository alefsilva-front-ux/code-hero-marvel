import { createGlobalStyle } from 'styled-components'
import { getSpacing } from './mixing'

const globalStyle = createGlobalStyle`
  *, *:before, *:after {
    color: #555555;
    box-sizing: border-box;
    font-family: "PT Sans", "Open Sans", "Roboto", -apple-system, BlinkMacSystemFont, "Segoe UI", sans-serif;
    -webkit-font-smoothing: antialiased;
    -moz-osx-font-smoothing: grayscale;
  }

  html,
  body
  {
    background-color: #E5E5E5;
    padding-bottom: ${({ theme }) => theme.spacing(5)}px;
  }

  body {
    margin: 0;
    line-height: normal;
  }

  a { color: #242424; }
  label, select, button { cursor: pointer; }
  img { max-width: 100%; }

  ${getSpacing('margin', 'm')}
  ${getSpacing('padding', 'p')}
`

export default globalStyle
