import axios from 'axios'
import md5 from 'md5'

const timestemp = Number(new Date())
const privateKey = process.env.REACT_APP_MARVEL_PRIVATE_KEY
const publicKey = process.env.REACT_APP_MARVEL_PUBLIC_KEY
const hash = md5(timestemp + privateKey + publicKey)

export default axios.create({
  baseURL: process.env.REACT_APP_MARVEL_BASE_URL,
  params: {
    apikey: publicKey,
    ts: timestemp,
    hash
  }
})
