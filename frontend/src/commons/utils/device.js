export const isIOS =
  !!global.navigator.userAgent && /iPad|iPhone|iPod/.test(global.navigator.userAgent)
export const isSafari = /^((?!chrome|android).)*safari/i.test(global.navigator.userAgent)
export const isMobile = isIOS || isSafari
