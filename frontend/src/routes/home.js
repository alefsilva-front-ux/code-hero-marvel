import { lazy } from 'react'

const HomeContainer = lazy(() => import('containers/Home'))

const home = [
  {
    path: '/',
    component: HomeContainer,
    exact: true
  }
]

export default home
