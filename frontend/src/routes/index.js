import home from './home'
import detail from './detail'

const routes = [...home, ...detail]

export default routes
