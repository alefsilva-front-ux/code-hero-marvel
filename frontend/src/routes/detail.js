import { lazy } from 'react'

const DetailContainer = lazy(() => import('containers/Detail'))

const detail = [
  {
    path: '/detalhes',
    component: DetailContainer,
    exact: true
  },
  {
    path: '/personagem/:id',
    component: DetailContainer,
    exact: true
  }
]

export default detail
