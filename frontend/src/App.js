import React, { Fragment } from 'react'
import { Router } from 'react-router-dom'
import WebFont from 'webfontloader'
import { ThemeProvider } from 'styled-components'
import { history } from './commons/utils/router'
import GlobalStyle from './commons/styles/global-style'
import Theme from './commons/constants/theme'
import Main from './containers/Main'

WebFont.load({
  google: {
    families: ['PT Sans:400,700', 'sans-serif']
  }
})

const App = () => (
  <ThemeProvider theme={Theme}>
    <Fragment>
      <GlobalStyle />
      <Router history={history}>
        <Main />
      </Router>
    </Fragment>
  </ThemeProvider>
)

export default App
