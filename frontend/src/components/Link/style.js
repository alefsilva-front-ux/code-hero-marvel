import styled, { css } from 'styled-components'
import { Link as RouterLink } from 'react-router-dom'

const LinkStyles = css`
  display: flex;
  text-decoration: none;
  cursor: pointer;

  :hover {
    text-decoration: underline;
  }
`

export const RouterLinkStyled = styled(RouterLink)`
  ${LinkStyles}
`

export const ExternalLinkStyled = styled.a`
  ${LinkStyles}
`
