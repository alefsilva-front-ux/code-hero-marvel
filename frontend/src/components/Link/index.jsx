import React from 'react'
import { RouterLinkStyled, ExternalLinkStyled } from './style'

const Link = (props) => {
  const { type, url } = props

  return type === 'external' ? (
    <ExternalLinkStyled {...props} href={url} target="_blank" />
  ) : (
    <RouterLinkStyled {...props} to={url} />
  )
}

export default Link
