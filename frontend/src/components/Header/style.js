import styled from 'styled-components'

export const WrapHeaderStyled = styled.div`
  display: flex;
  background: #fff;
  padding: ${({ theme }) => theme.spacing(1)}px ${({ theme }) => theme.spacing(3)}px;
  margin-bottom: ${({ theme }) => theme.spacing(2)}px;

  p {
    display: inline-block;
    padding: 0 ${({ theme }) => theme.spacing(2)}px;

    @media only screen and (max-width: 768px) {
      font-size: 0.75rem;

      strong {
        display: flex;
      }
    }
  }
`

export const LogoStyled = styled.img`
  width: 96px;

  @media only screen and (max-width: 599px) {
    display: flex;
    width: 100%;
    max-width: 170px;
  }
`

export const UserInitialsStyled = styled.span`
  width: 32px;
  height: 32px;
  cursor: pointer;
  padding: ${({ theme }) => theme.spacing(1)}px;
  font-weight: 700;
  background: #f5f5f5;
  border-radius: 4px;
  transition: all 0.4s ease 0s;

  :hover {
    background: #555555;
    color: #fff;
  }
`
