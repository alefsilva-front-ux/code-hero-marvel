import React from 'react'
import logo from 'assets/svg/logo.svg'
import { Link } from 'components'
import { Grid } from '@material-ui/core'
import { WrapHeaderStyled, LogoStyled, UserInitialsStyled } from './style'

const Header = () => {
  return (
    <>
      <WrapHeaderStyled>
        <Grid
          container
          direction="row"
          justifyContent="space-between"
          alignItems="center"
        >
          <Grid item xs={4} sm={3} md={4}>
            <Link url="/">
              <LogoStyled src={logo} alt="Logotipo Objective" />
            </Link>
          </Grid>
          <Grid item xs={7} md={7} align="right">
            <p>
              <strong>Nome do candidato</strong> Teste de Front-end
            </p>
          </Grid>
          <Grid item xs={1}>
            <UserInitialsStyled>CB</UserInitialsStyled>
          </Grid>
        </Grid>
      </WrapHeaderStyled>
    </>
  )
}

export default Header
