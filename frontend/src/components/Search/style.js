import styled from 'styled-components'
import { Grid } from '@material-ui/core'
import SearchIcon from '@material-ui/icons/Search'

export const WrapSearchStyled = styled.div`
  display: flex;
  flex-direction: column;

  h1 {
    font-size: 2.3125rem;
    font-weight: 700;
    margin-top: ${({ theme }) => theme.spacing(2)}px;
    margin-bottom: ${({ theme }) => theme.spacing(1)}px;

    @media only screen and (max-width: 768px) {
      font-size: 1.5rem;
    }
  }

  label {
    margin-top: 0;
    margin-bottom: ${({ theme }) => theme.spacing(1)}px;
    font-size: 1.125rem;
    font-weight: 700;

    @media only screen and (max-width: 768px) {
      font-size: 0.875rem;
    }
  }

  input {
    width: 295px;
    height: 32px;
    background: #ffffff 0% 0% no-repeat padding-box;
    border: 1px solid #e5e5e5;
    border-radius: 4px;
    padding: 0 ${({ theme }) => theme.spacing(5)}px 0 ${({ theme }) => theme.spacing(2)}px;

    ::placeholder {
      font-style: italic;
    }

    @media only screen and (max-width: 768px) {
      width: 100%;
      border-right: 0;
      border-top-right-radius: 0;
      border-bottom-right-radius: 0;
    }
  }

  @media only screen and (max-width: 768px) {
    text-align: center;
  }
`

export const WrapSeachStyled = styled.div`
  width: 295px;

  @media only screen and (max-width: 768px) {
    width: 100%;
  }
`

export const SearchIconStyled = styled(SearchIcon)`
  margin-top: 6px;
  border-top-right-radius: 4px;
  border-bottom-right-radius: 4px;

  path {
    color: #555555a6;
  }
`

export const WrapSearchIconStyled = styled(Grid)`
  @media only screen and (max-width: 768px) {
    background: #fff;
    border: 1px solid #e5e5e5;
    border-left: none;
    border-right: none;
    border-top-right-radius: 4px;
    border-bottom-right-radius: 4px;
  }
`
