import React, { useState, useEffect } from 'react'
import { Grid } from '@material-ui/core'
import { getCharactersByName } from 'services/character.service'
import {
  WrapSearchStyled,
  SearchIconStyled,
  WrapSeachStyled,
  WrapSearchIconStyled
} from './style'

const Search = ({ handleUpdateHeroes, resetSearch, searchTerm, setSearchTerm }) => {
  const [lastSearchTerm, setLastSearchTerm] = useState('')

  useEffect(() => {
    if (searchTerm && searchTerm.length > 2) {
      setTimeout(() => {
        if (searchTerm !== lastSearchTerm) {
          getCharactersByName(searchTerm).then((response) => {
            handleUpdateHeroes(response)
          })
          setLastSearchTerm(searchTerm)
        }
      }, 3000)
    } else {
      resetSearch()
    }
    // eslint-disable-next-line react-hooks/exhaustive-deps
  }, [searchTerm])

  const handleChange = (event) => {
    setSearchTerm(event.target.value)
  }

  return (
    <WrapSearchStyled>
      <h1>Busca de personagens</h1>
      <label htmlFor="search">Nome do personagem</label>
      <WrapSeachStyled>
        <Grid container>
          <Grid item xs={10}>
            <input
              id="search"
              placeholder="Search"
              value={searchTerm}
              onChange={handleChange}
            />
          </Grid>
          <WrapSearchIconStyled item xs={2} align="center">
            <SearchIconStyled fontSize="small" />
          </WrapSearchIconStyled>
        </Grid>
      </WrapSeachStyled>
    </WrapSearchStyled>
  )
}
export default Search
