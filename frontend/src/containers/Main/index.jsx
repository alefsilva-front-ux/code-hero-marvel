import React, { memo, Suspense } from 'react'
import { Route, Switch, Redirect } from 'react-router-dom'
import routes from 'routes'

function Main() {
  return (
    <>
      <Suspense fallback={'Carregando...'}>
        <Switch>
          {routes.map((route) => (
            <Route key={route.path} {...route} />
          ))}
          <Redirect from="*" to="/" />
        </Switch>
      </Suspense>
    </>
  )
}

export default memo(Main)
