import React, { memo, useEffect, useState } from 'react'
import SEO from './SEO'
import { Container, Grid } from '@material-ui/core'
import { Header, Search } from 'components'
import ListHeroes from './components/ListHeroes'
import Footer from './components/Footer'
import { getCharacters, getCharacterWithSearchTerm } from 'services/character.service'

function Home() {
  const [heroes, setHeroes] = useState([])
  const [pagination, setPagination] = useState({ count: 0, offset: 0, total: 0 })
  const [currentPage, setCurrentPage] = useState(1)
  const [searchTerm, setSearchTerm] = useState('')
  const ListLimit = 10

  const resetSearch = () => {
    getCharacters().then((response) => {
      if (response.data) {
        const data = response.data.data
        const newHeroes = data.results
        const { count, total } = data
        setPagination({
          count,
          offset: 0,
          total
        })
        setCurrentPage(1)
        setHeroes(newHeroes)
      }
    })
  }

  useEffect(() => {
    try {
      resetSearch()
    } catch (error) {
      console.log('error:', error)
    }
  }, [])

  const handleUpdateHeroes = (response) => {
    if (response.data) {
      const newHeroes = response.data.data.results
      setHeroes(newHeroes)
    }
  }

  const handleUpdateSearch = (response) => {
    const { count, total } = response.data.data
    setCurrentPage(1)
    setPagination({
      count,
      offset: 0,
      total
    })
    handleUpdateHeroes(response)
  }

  useEffect(() => {
    getCharacterWithSearchTerm(pagination.offset, searchTerm).then((response) => {
      handleUpdateHeroes(response)
    })
    // eslint-disable-next-line react-hooks/exhaustive-deps
  }, [pagination])

  const setOffset = (value) => value * ListLimit

  const handlePaginationChange = (_, value) => {
    const newOffset = value > 1 ? setOffset(value - 1) : 0
    setCurrentPage(value)
    setPagination({ ...pagination, offset: newOffset })
  }

  return (
    <>
      <SEO />
      <Header />
      <Container maxWidth="md">
        <Grid container direction="row" spacing={2}>
          <Grid item xs={12}>
            <Search
              handleUpdateHeroes={handleUpdateSearch}
              resetSearch={resetSearch}
              searchTerm={searchTerm}
              setSearchTerm={setSearchTerm}
            />
            <ListHeroes heroes={heroes} />
          </Grid>
        </Grid>
      </Container>
      {pagination.total > ListLimit && (
        <Footer
          pagination={pagination}
          handlePaginationChange={handlePaginationChange}
          currentPage={currentPage}
        />
      )}
    </>
  )
}

export default memo(Home)
