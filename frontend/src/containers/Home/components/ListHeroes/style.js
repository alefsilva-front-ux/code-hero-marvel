import styled from 'styled-components'
import { Grid } from '@material-ui/core'
import { Link } from 'components'

export const WrapHeaderStyled = styled(Grid)`
  padding: 0 ${({ theme }) => theme.spacing(3)}px;

  p {
    font-size: 0.75rem;
    color: #8e8e8e;

    @media only screen and (max-width: 768px) {
      padding-left: ${({ theme }) => theme.spacing(9)}px;
    }
  }
`

export const WrapItemStyled = styled(Grid)`
  background: #fff;
  box-shadow: 0px 0px 5px #00000033;
  border-radius: 4px;
  padding: ${({ theme }) => theme.spacing(2)}px ${({ theme }) => theme.spacing(3)}px;
  margin-bottom: ${({ theme }) => theme.spacing(1)}px;
  transition: all 0.4s ease 0s;

  img {
    width: 48px;
  }

  span,
  strong,
  p {
    font-size: 0.875rem;
  }

  :hover {
    box-shadow: 0px 0px 5px #000000;
  }
`

export const LinkStyled = styled(Link)`
  :hover {
    text-decoration: none;
  }
`

export const WrapHeroImageStyled = styled.div`
  display: inline-block;
  width: 48px;
  height: 48px;
  border-radius: 4px;
  overflow: hidden;
  margin-right: ${({ theme }) => theme.spacing(3)}px;
`
