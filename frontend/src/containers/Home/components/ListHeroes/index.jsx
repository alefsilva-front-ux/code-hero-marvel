import React from 'react'
import { Grid, Hidden } from '@material-ui/core'
import {
  WrapHeaderStyled,
  WrapItemStyled,
  LinkStyled,
  WrapHeroImageStyled
} from './style'

const ListHeroes = ({ heroes }) => {
  const renderHeader = () => (
    <WrapHeaderStyled container>
      <Grid item xs={4}>
        <p>Personagem</p>
      </Grid>
      <Hidden xsDown>
        <Grid item xs={4}>
          <p>Séries</p>
        </Grid>
        <Grid item xs={4}>
          <p>Eventos</p>
        </Grid>
      </Hidden>
    </WrapHeaderStyled>
  )

  const renderItems = () =>
    heroes.map(({ id, thumbnail: { path, extension }, name, series, events }, key) => (
      <LinkStyled url={`/personagem/${id}`} key={key}>
        <WrapItemStyled container alignContent="center" alignItems="center">
          <Grid item xs={12} sm={4}>
            <Grid container alignItems="center">
              <WrapHeroImageStyled>
                <img src={`${path}.${extension}`} alt={name} />
              </WrapHeroImageStyled>
              <strong> {name}</strong>
            </Grid>
          </Grid>
          <Hidden xsDown>
            <Grid item xs={12} sm={4}>
              {series.items
                .map(({ name }, key) => (
                  <span key={key}>
                    {name}
                    <br />
                  </span>
                ))
                .filter((_, key) => key <= 2)}
            </Grid>
            <Grid item xs={12} sm={4}>
              {events.items
                .map(({ name }, key) => (
                  <span key={key}>
                    {name}
                    <br />
                  </span>
                ))
                .filter((_, key) => key <= 2)}
            </Grid>
          </Hidden>
        </WrapItemStyled>
      </LinkStyled>
    ))

  return (
    <div className="mt-2">
      {renderHeader()}
      {heroes.length > 0 ? (
        renderItems()
      ) : (
        <>
          <Grid container>
            <Grid item xs={12}>
              <p>Nenhum resultado encontrado.</p>
            </Grid>
          </Grid>
        </>
      )}
    </div>
  )
}

export default ListHeroes
