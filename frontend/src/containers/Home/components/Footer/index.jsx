import React from 'react'
import { Container, Grid } from '@material-ui/core'
import { WrapperStyled, PaginationStyled } from './style'
import { isMobile } from 'commons/utils/device'

const Footer = ({ pagination, handlePaginationChange, currentPage }) => {
  const { total } = pagination
  const ListLimit = 10
  const countOfTotal = Number((total / ListLimit).toFixed())

  return (
    <WrapperStyled>
      <Container maxWidth="md">
        <Grid container direction="column" alignContent="center" alignItems="center">
          <Grid item xs={12}>
            <PaginationStyled
              count={total >= ListLimit ? countOfTotal + 1 : countOfTotal}
              page={currentPage}
              showFirstButton
              showLastButton
              onChange={handlePaginationChange}
              siblingCount={isMobile ? 0 : 1}
            />
          </Grid>
        </Grid>
      </Container>
    </WrapperStyled>
  )
}

export default Footer
