import styled from 'styled-components'
import { Pagination } from '@material-ui/lab'

export const WrapperStyled = styled.div`
  display: flex;
  position: fixed;
  width: 100%;
  bottom: 0;
  padding: ${({ theme }) => theme.spacing(2)}px 0;
  background: #fff;
  border-top: 1px solid #e5e5e5;
`

export const PaginationStyled = styled(Pagination)`
  .MuiPaginationItem-page.Mui-disabled {
    display: none;
  }

  .MuiPaginationItem-page {
    border: 1px solid #e5e5e5;
    background: #f5f5f5;
    color: #555555;
    border-radius: 4px;

    :hover {
      background-color: #167abc90;
      color: #fff;
    }

    &[aria-label='Go to first page'],
    &[aria-label='Go to previous page'],
    &[aria-label='Go to next page'],
    &[aria-label='Go to last page'] {
      background: none;
      border: none;
    }

    @media only screen and (max-width: 768px) {
      min-width: 30px;
      border-radius: 4px;
      padding: 0;
      margin: 0 0.6px;
    }
  }

  .MuiPaginationItem-page.Mui-selected {
    background-color: #167abc;
    color: #fff;
  }

  .MuiPaginationItem-icon {
    font-size: 1rem;
  }
`
