import React, { memo, useEffect, useState } from 'react'
import { Header, Link } from 'components'
import { Container, Grid } from '@material-ui/core'
import { useParams } from 'react-router-dom'
import { getCharacterById, getCharacterEventsById } from 'services/character.service'
import SEO from './SEO'
import {
  WrapHeroHeaderStyled,
  ImageStyled,
  ArrowBackIosRoundedIconStyled,
  WrapInsideContentStyled,
  WrapMainHeroDataStyled,
  WrapEventStyled
} from './style'

function Detail() {
  const { id } = useParams()
  const [hero, setHero] = useState()
  const [heroEvents, setHeroEvents] = useState([])

  useEffect(() => {
    try {
      getCharacterById(id).then((response) => {
        if (response.data) {
          const newHero = response.data.data.results[0]
          setHero(newHero)
        }
      })
      getCharacterEventsById(id).then((response) => {
        if (response.data) {
          setHeroEvents(response.data.data.results)
        }
      })
    } catch (error) {
      console.log('error:', error)
    }
  }, [id])

  const renderHeroHeader = () => {
    const {
      thumbnail: { path, extension },
      name,
      series,
      events
    } = hero

    return (
      <WrapHeroHeaderStyled>
        <Container maxWidth="md">
          <WrapInsideContentStyled>
            <ImageStyled src={`${path}.${extension}`} alt={name} className="mr-4" />
            <WrapMainHeroDataStyled>
              <Link url="/">
                <ArrowBackIosRoundedIconStyled fontSize="large" className="mb-2" />
              </Link>
              <h1>{name}</h1>
              <p>
                Séries: {series.items.length}
                <br />
                Eventos: {events.items.length}
              </p>
            </WrapMainHeroDataStyled>
          </WrapInsideContentStyled>
        </Container>
      </WrapHeroHeaderStyled>
    )
  }

  const renderSeries = (series) => (
    <Grid container>
      <Grid item xs={12}>
        <h2>Séries</h2>
        <ul>
          {series.items.map(({ name }, key) => (
            <li key={key}>{name}</li>
          ))}
        </ul>
      </Grid>
    </Grid>
  )

  const renderEvents = () => (
    <>
      <Grid container>
        <Grid item xs={12}>
          <h2>Eventos</h2>
        </Grid>
      </Grid>
      <Grid container justifyContent="center">
        {heroEvents.map(({ title, description, thumbnail: { path, extension } }, key) => (
          <Grid item xs={12} sm={6} md={4} key={key} className="mb-3">
            <WrapEventStyled>
              <h3>{title}</h3>
              <img src={`${path}.${extension}`} alt={title} />
              <p>{description}</p>
            </WrapEventStyled>
          </Grid>
        ))}
      </Grid>
    </>
  )

  const renderHeroInformation = () => {
    const { series } = hero
    return (
      <>
        {series.items.length > 0 && renderSeries(series)}
        {heroEvents.length > 0 && renderEvents()}
      </>
    )
  }

  return (
    <>
      <SEO />
      <Header />
      {hero && renderHeroHeader()}
      <Container maxWidth="md">
        {hero ? renderHeroInformation() : <p>Carregando...</p>}
      </Container>
    </>
  )
}

export default memo(Detail)
