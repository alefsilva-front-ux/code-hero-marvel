import styled from 'styled-components'
import ArrowBackIosRoundedIcon from '@material-ui/icons/ArrowBackIosRounded'

export const WrapHeroHeaderStyled = styled.div`
  background: #555555;
  padding-top: ${({ theme }) => theme.spacing(2)}px;
  padding-bottom: ${({ theme }) => theme.spacing(2)}px;

  h1 {
    font-size: 3.75rem;
    margin: 0;

    @media only screen and (max-width: 768px) {
      font-size: 2.5rem;
    }
  }

  p,
  h1 {
    color: #fff !important;
  }
`

export const ImageStyled = styled.img`
  width: 340px;
  border: 10px dashed #fff;
  border-radius: 4px;

  @media only screen and (max-width: 768px) {
    margin-bottom: ${({ theme }) => theme.spacing(4)}px;
  }

  @media only screen and (max-width: 630px) {
    margin-right: 0 !important;
  }
`

export const ArrowBackIosRoundedIconStyled = styled(ArrowBackIosRoundedIcon)`
  border-radius: 50%;
  background: #333;
  text-align: center;
  padding: ${({ theme }) => theme.spacing(1)}px;
  transition: all 0.4s ease 0s;
  cursor: pointer;

  path {
    transition: all 0.4s ease 0s;
    color: #fff;
  }

  :hover {
    background: #fff;

    path {
      color: #333;
    }
  }
`

export const WrapMainHeroDataStyled = styled.div`
  display: flex;
  flex-direction: column;
  justify-content: center;
`

export const WrapInsideContentStyled = styled.div`
  display: grid;
  width: fit-content;
  grid-template-columns: max-content 1fr;
  align-self: center;
  margin: 0 auto;

  @media only screen and (max-width: 768px) {
    display: flex;
    flex-wrap: wrap;
    justify-content: center;
  }
`

export const WrapEventStyled = styled.div`
  width: 90%;
  height: 100%;
  display: flex;
  flex-direction: column;
  flex-wrap: wrap;
  background: #fff;
  border-radius: 4px;
  padding: ${({ theme }) => theme.spacing(2)}px ${({ theme }) => theme.spacing(3)}px;
  margin-bottom: ${({ theme }) => theme.spacing(1)}px;
  margin: 0 auto;
`
