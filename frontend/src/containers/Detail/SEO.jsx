import React from 'react'
import { Helmet } from 'react-helmet'
import Thumbnail from 'assets/png/thumbnail.png'

function SEO() {
  return (
    <Helmet
      title="Objective | Detalhes do Herói"
      meta={[
        {
          name: 'description',
          content: 'Objective'
        },
        {
          property: 'og:url',
          content: 'https://www.objective.com.br/'
        },
        {
          property: 'og:image',
          content: Thumbnail
        },
        {
          property: 'og:image:width',
          content: '1200'
        },
        {
          property: 'og:image:height',
          content: '628'
        }
      ]}
    />
  )
}

export default SEO
