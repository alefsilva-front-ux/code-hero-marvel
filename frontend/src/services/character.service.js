import http from 'commons/utils/http'

const entity = 'characters'
const listLimit = 10

const getCharacters = async () => {
  const url = `${entity}?limit=${listLimit}`
  return await http.get(url)
}

const getCharactersByName = async (searchTerm) => {
  const url = `${entity}?nameStartsWith=${searchTerm}&limit=${listLimit}`
  return await http.get(url)
}

const getCharacterById = async (id) => {
  const url = `${entity}/${id}`
  return await http.get(url)
}

const getCharacterEventsById = async (id) => {
  const url = `${entity}/${id}/events`
  return await http.get(url)
}

const getCharacterFromPagination = async (offset) => {
  const url = `${entity}?offset=${offset}&limit=${listLimit}`
  return await http.get(url)
}

const getCharacterWithSearchTerm = async (offset, searchTerm) => {
  const url =
    searchTerm && searchTerm.length > 2
      ? `${entity}?nameStartsWith=${searchTerm}&offset=${offset}&limit=${listLimit}`
      : `${entity}?offset=${offset}&limit=${listLimit}`

  return await http.get(url)
}

export {
  getCharacters,
  getCharactersByName,
  getCharacterById,
  getCharacterEventsById,
  getCharacterFromPagination,
  getCharacterWithSearchTerm
}
