# Getting Started with Create React App

This project was bootstrapped with [Create React App](https://github.com/facebook/create-react-app).

## Add the .env file

You can use the .env.sample as example using your Marvel API keys.

## Available Scripts

In the project directory, you can run:

### `npm install`

### `npm start`

##

Runs the app in the development mode.\
Open [http://localhost:3000](http://localhost:3000) to view it in the browser.

## Screenshots

### Home Desktop

![Scheme](../screenshots/screenshot-home-desktop.png)

### Home Mobile

![Scheme](../screenshots/screenshot-home-mobile.png)

### Detail Desktop

![Scheme](../screenshots/screenshot-detail-desktop.png)

### Detail Mobile

![Scheme](../screenshots/screenshot-detail-mobile.png)
